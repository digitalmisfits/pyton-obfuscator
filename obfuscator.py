#!/usr/bin/python

import zlib
import binascii
import sys

import string
import ast
from ast import Module, Assign, Name, Call, Store, Load, Str, Num, List, Add, Sub, BinOp, Expr, ImportFrom, Param
from ast import Subscript, Slice, Attribute, GeneratorExp, comprehension
from ast import Compare, Mult, BoolOp, And, Or, Lt, LtE, NotEq, Expr, Index, Tuple, For, Delete, Del, Dict
from ast import alias
import unparse
import random
import time
import StringIO
from string import ascii_lowercase, digits

def mangle(s):


	t = [ 
		lambda x: Subscript(
			value = Str(s = x[::-1]), 
			slice = Slice(lower = None, upper = None, step = Num(n = -1)), 
			ctx=Load()),

		#lambda x: Str(s = x.encode('unicode-escape').encode('rot13')),

		lambda x: Call(func = Attribute(value=Name(id = 'zlib', ctx=Load()), attr = 'decompress', ctx = Load()), 
				args = [
					Call(func = Attribute(value=Name(id = 'binascii', ctx=Load()), attr = 'unhexlify', ctx = Load()),
						args = [Str(s = binascii.hexlify(zlib.compress(x, 9)))], keywords = [], starargs = None, kwargs = None)
				], keywords = [], starargs = None, kwargs = None)
	]

	return random.choice(t)(s)


def obfname():
	return ''.join(random.choice(ascii_lowercase) for _ in range(24))


def call_load(id = None, args = []):
	return Call(func = Name(id = id, ctx = Load()), args = args, keywords = [], starargs = None, kwargs = None)


def call_globals():
	return call_load('globals', args = [])


def call_locals():
	return call_load('locals', args = [])


def shuffle(l):
	if isinstance(l, list):
		t = l[:]
		random.shuffle(t)
		return t
	return l


class Obfuscator(ast.NodeTransformer):

	def __init__(self):
		ast.NodeTransformer.__init__(self)
	
		self.scope = None
		self.parent = None
		self.imports = {}
		self.modules = {}

		self.depth = -1
		self.test = shuffle([])

	def visit_Module(self, node):
		self.generic_visit(node)
		return node


	def visit_Import(self, node):	

		return node

		nodes = []	

		for y in node.names:
			self.imports[y.name] = n = obfname()

			nodes[len(nodes):]  = [Assign(
				targets = [
					Subscript(
						value = call_load('globals'), slice = Index(value = Str(s = n)))
				],
				value = call_load('__import__', args = [Str(y.name), call_globals(), call_locals(), List(elts = []), Num(n = -1)]))]

	

		# serialize
		serialized = StringIO.StringIO()
		unparse.Unparser(nodes, serialized)

		# exec(serialized)
		node =  Expr(Call(func = Name(id='exec', ctx=Load()), args=[
				Str(serialized.getvalue()), call_globals(), call_locals()], keywords = [], starargs = None, kwargs = None))

		return node

	def visit_ImportFrom(self, node):

		node = ImportFrom(module = node.module, names=[alias(name = y.name, asname = y.name) for y in node.names], level = node.level)

		modules		= [y.name for y in node.names]
		modules_s	= [obfname() for y in node.names]

		for i, y in enumerate(modules):
			self.modules[y] = modules_s[i]


		# m = {n: [a,b,c], a: [b,c,a]}
		assign_m = Assign(targets=[Name(id='m', ctx=Store())], value=[Dict(
				keys 	= [Str(s = 'n'), Str(s = 'a')], 
				values 	= [
					List([Str(y) for y in modules], Load()), 
					List([Str(y) for y in modules_s], Load())])])

		# t = __import__(name, globals(), locals(), m.n, -1)
		assign_t =  Assign(
			targets = [Name(id='l', ctx=Store())],
			value 	= call_load('__import__', args = [Str(node.module), call_globals(), call_locals(), Name(id='m', ctx=Load()), Num(n = -1)]))

		assign_lt = [
			assign_m, 
			assign_t
		]


		#for i, y in enumerate(m.n): globals()[y] = getattr(l, y)

		var_i = Name(id = 'i', ctx = Load())
		var_y = Name(id = 'y', ctx = Load())

		call_enumerate 	= call_load('enumerate', args = [Subscript(value = Name(id='m', ctx=Load()), slice = Index(value = Str(s='n')))])
		call_getattr 	= call_load('getattr', args = [Name(id = 'l', ctx = Load()), Name(id = 'y', ctx = Load())])

		#for i, y in enumerate(m.n): globals()[m[a][y] = getattr(l, y)
		assign_g = For(target = Tuple(elts=[var_i, var_y]), iter = call_enumerate, 
			body = [Assign(
				targets = [	
					Subscript(
						value = call_load('globals'), 
						slice = Index(
							value = Subscript(
								value = Subscript(value = Name(id='m', ctx = Load()), slice = Index(Str(s = 'a'))),
								slice = Index(value = Name(id = 'i', ctx = Load()))
							)
						)
					)
				], 
				value = call_getattr)
		], orelse = None)

		# del l, m
		del_l = Delete(targets = [Name(id = 'l', ctx = Del()), Name(id = 'm', ctx = Del())])

		#unparse.Unparser([assign_lt, assign_g, del_l], sys.stdout)
		#sys.exit()
		
		# serialize
		serialized = StringIO.StringIO()
		unparse.Unparser([assign_lt, assign_g, del_l], serialized)
		
		# exec(serialized)
		node =  Expr(Call(func = Name(id='exec', ctx=Load()), args=[
				Str(serialized.getvalue()), call_globals(), call_locals()], keywords = [], starargs = None, kwargs = None))		

		return self.generic_visit(node)

	def visit_Str(self, node):
		# Attribute zlib.decompress -> getattr('x', 'y') --> visit -> getattr(zlib.decompress('x'), zlib.decompress('y')) --> loop

		if self.depth > 1:
			return Str(node.s)

		node =  mangle(node.s)
		if isinstance(node, Call):
			self.visit(node)

		return node


	def visit_Call(self, node):
		self.depth += 1
		self.generic_visit(node)
		self.depth -= 1

		return node


	def visit_Num(self, node):
		if isinstance(node.n, (int,long)):
			d = random.randint(1, 64)

			#evaluate = Subscript(
			#	value = Call(func = Name(id = 'len', ctx = Load()), 
			#		args = [Attribute(value = Name(id = 'sys', ctx = Load()), attr='argv', ctx = Load())],
			#		keywords = [], starargs = None, kwargs = None),
			#	slice = Slice(lower = None, upper = None, step = Num(n = -1)))

			evaluate =  Call(func = Name(id = 'len', ctx = Load()), 
					args = [Attribute(value = Name(id = 'sys', ctx = Load()), attr='argv', ctx = Load())],
					keywords = [], starargs = None, kwargs = None)
			
			return 	BinOp(left = BinOp(left = Num(node.n / d), op = Mult(), right = Num(n = d)),
				op 	= Add(), 	
				right 	= Num(node.n % d))
			

			#return BinOp(
			#	left 	= BinOp(left = Num(node.n / d), op = Mult(), right = Num(n = d)),
			#	op 	= Add(), 	
			#	right 	= BinOp(
			#			left 	= evaluate,
			#			op 	= Sub(),
			#			right 	= BinOp(left = evaluate, op = Add(), right = Num(node.n % d))
			#		)
			#	)

		return node


	def visit_If(self, node):
		opaque = Compare(left = Num(n = 0), ops = [NotEq(),], comparators = [
			Expr(value=Call(
				func = Attribute(value=Name(id='time', ctx=Load()), attr='clock', ctx=Load()), args = [], keywords = [], starargs = None, kwargs = None)
			)])

		node.test = BoolOp(op = And(), values = [node.test, opaque])

		for i, y in enumerate(node.test.values):
			node.test.values[i] = self.generic_visit(y)

		for i, y in enumerate(node.body):
			node.body[i] = self.generic_visit(y)	

		return node


	def visit_Name(self, node):

		if isinstance(node.ctx, Load) and node.id in self.modules:
			#print "%s, %s" % (node.id, self.imports[node.id])
			node.id = self.modules[node.id]	

		return node


	def visit_Attribute(self, node):

		if isinstance(node.value, Name) and isinstance(node.ctx, Load):
			id = self.imports[node.value.id] if node.value.id in self.imports else node.value.id

			node = Call(func=Name(id='getattr', ctx=Load()), args=[
	        		Name(id = id, ctx=Load()), Str(s=node.attr)],keywords=[], starargs=None, kwargs=None)
			return self.visit(node)

		return self.generic_visit(node)
			

if __name__ == '__main__':

	obfuscator = Obfuscator()

	with open(__file__, 'rb') as f :
		root = ast.parse(f.read())

	transformed = obfuscator.visit(root);

	#for y in range(5):
	#	transformed = obfuscator.visit(transformed);
	
	unparse.Unparser(transformed, sys.stdout)


