import web
import sys
import ast
import unparse
import StringIO
from obfuscator import Obfuscator

web.config.debug = False        
render = web.template.render('pages/')

urls = (	
	'/', 'index',
	'/transform', 'transform'
)

class transform:
	def POST(self):
        	x = web.input(file={})

	        obfuscator = Obfuscator()
		try:
        		root = ast.parse(x['file'].file.read())
	        except:
			raise web.internalerror()
			
		transformed = obfuscator.visit(root);

		if int(x['iterations']) in range(5 + 1):
			for y in range(int(x['iterations']) - 1):
				transformed = obfuscator.visit(transformed);

		output = StringIO.StringIO()
        	unparse.Unparser(transformed, output)
        	return output.getvalue()

		#raise web.seeother('/transform')
      
class index:
	def GET(self):
		return render.index()

#if __name__ == "__main__":
#	app.run()

app = web.application(urls, globals()).wsgifunc()
